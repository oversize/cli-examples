# Learning and playing with go clis

I wanted to learn go. So i wrote some go. Actually i intend to write more
go in the future here. Anyways... if you already know go, you probably
dont find anything usefull here.

## GO's flag package

https://golang.org/pkg/flag/

**Arguments, Options, Flags**

There is no clear definition. But the somewhat general differences:

* Arguments - Everything after the command
* Options - Arguments that have dashes (-, --) and take user input
* Flags - Booleans (Options without user input; they just exist, or not)

> GOs flag package does not stick to these. Everything with dashses is considered
> a flag (with or withour user input). All strings without dashes (that are not
> user input for flags) are considered arguments.

Flags can be defined using flag.String(), Bool(), Int(), etc.

    examplePtr := flag.String("example", "defaultValue", " Help text.")

Now, *examplePtr* will reference the value for either *-example* or *--example*.
The initial value at *examplePtr is “defaultValue”. Calling flag.Parse() will
parse the command line input and write the value following -example or
--example to *examplePtr.

Flag Syntax examples:

    --example
    --example value
    --example "value  in quotes"
    --example="value in quotes with ="

See package docs for more examples: https://golang.org/pkg/flag/#hdr-Command_line_flag_syntax

After parsing the Arguments all arguments (and flags) are stored in *flag.Args()*
or *flag.Arg(i)*.

**Errors** - are handley by the parser. E.g. required values, type mismatches and so on
Print usage with *flag.PrintDefaults()*

**Test if flags** - are set by comparing the value with the default value.
Easy for booleans, not so much for ints and floats.

**Make flag required** - by checking if set and bail out manually.

**Make a choice flag** - by testing the value against a specified set.

**Sub Commands**

Subcommands are created using a *flag.FlagSet*


**... To be continued ...**


## Sources

* https://blog.rapid7.com/2016/08/04/build-a-simple-cli-tool-with-golang/
* https://blog.alexellis.io/5-keys-to-a-killer-go-cli/
* https://carlosbecker.com/posts/golang-cli-apps/


### Alternativ ufve/cli

An alternative to the flag package

* https://github.com/urfave/cli
* https://tutorialedge.net/golang/building-a-cli-in-go/