package main

import (
	"flag"
	"fmt"
	"os"
)

/*
 */
func oldMain() {
	textPtr := flag.String("text", "", "Text to parse.")
	metricPtr := flag.String("metric", "chars", "Metric {chars|words|lines};.")
	// Bool flags will be true if given. If default is false
	uniquePtr := flag.Bool("unique", false, "Measure unique values of a metric.")

	// After the flags are defined, you need to parse the arguments
	flag.Parse()

	// If textPtr holds default value, it was not given.
	if *textPtr == "" {
		// Require the flag by quitting here. There is no automagic for that in flag pkg
		flag.PrintDefaults()
		os.Exit(1)
	}

	fmt.Printf("textPtr: %s, metricPtr: %s, uniquePtr: %t\n", *textPtr, *metricPtr, *uniquePtr)

	flag.PrintDefaults()
	// fmt.Println(flag.Args())
}

func main() {
	// Subcommands
	countCommand := flag.NewFlagSet("count", flag.ExitOnError)
	listCommand := flag.NewFlagSet("list", flag.ExitOnError)

	// Count subcommand flag pointers
	// Adding a new choice for --metric of 'substring' and a new --substring flag
	countTextPtr := countCommand.String("text", "", "Text to parse. (Required)")
	//countMetricPtr := countCommand.String("metric", "chars", "Metric {chars|words|lines|substring}. (Required)")
	//countSubstringPtr := countCommand.String("substring", "", "The substring to be counted. Required for --metric=substring")
	//countUniquePtr := countCommand.Bool("unique", false, "Measure unique values of a metric.")

	// List subcommand flag pointers
	listTextPtr := listCommand.String("text", "", "Text to parse. (Required)")
	//listMetricPtr := listCommand.String("metric", "chars", "Metric <chars|words|lines>. (Required)")
	//listUniquePtr := listCommand.Bool("unique", false, "Measure unique values of a metric.")

	// Verify that a subcommand has been provided
	// os.Arg[0] is the main command
	// os.Arg[1] will be the subcommand
	if len(os.Args) < 2 {
		fmt.Println("list or count subcommand is required")
		os.Exit(1)
	}

	// Switch on the subcommand
	// Parse the flags for appropriate FlagSet
	// FlagSet.Parse() requires a set of arguments to parse as input
	// os.Args[2:] will be all arguments starting after the subcommand at os.Args[1]
	switch os.Args[1] {
	case "list":
		listCommand.Parse(os.Args[2:])
	case "count":
		countCommand.Parse(os.Args[2:])
	default:
		flag.PrintDefaults()
		os.Exit(1)
	}

	if listCommand.Parsed() {
		fmt.Println(*listTextPtr)
	}

	if countCommand.Parsed() {

		fmt.Println(*countTextPtr)
	}
}
